\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC240: Ordering}
\author{Jad Elkhaleq Ghalayini}
\date{February 12 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{tabto}

\def\nats {{\mathbb N}}
\def\ints {{\mathbb Z}}
\def\reals {{\mathbb R}}
\newcommand{\iimp}{\mbox{ IMPLIES }}
\newcommand{\oor}{\mbox{ OR }}
\newcommand{\aand}{\mbox{ AND }}
\newcommand{\nnot}{\mbox{ NOT }}
\newcommand{\iiff}{\mbox{ IFF }}
\newcommand{\sless}{S}

\newtheorem{theorem}{Theorem}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

A set \(S\) is \underline{partially ordered} if there exists a binary predicate \(R: S \times S \to \{T, F\}\) such that \(\forall x \in S,\forall y \in S, \forall z \in S\),
\begin{itemize}
    \item [\(R(x, x) = T\)] (reflexivity)
    \item [\(R(x, y) \aand R(y, x) \implies x = y\)] (assymetric)
    \item [\(R(x, y) \aand R(y, z) \implies R(x, z)\)] (transitivity)
\end{itemize}
Examples:
\begin{itemize}
    \item [\(=\)] is a partial order for any set
    \item [\(<\)] is not a partial order for \(\reals\), since it is not reflexive
    \item [\(\leq\)] is a partial order for \(\reals\)
    \item [\(\leq\)] is not a partial order for \(\mathbb{C}\), since \(|i| = |1|\) but \(i \neq 1\).
    \item [\(divides(x, y)\)] is a partial order for \(\mathbb{Z}^+\)
\end{itemize}
\(S\) is totally ordered if there exists a partial order \(R\) on \(S\) such that \(\forall x, y \in S\),
\[R(x, y) \oor R(y, x) \text{ (comparability)}\]
\(R\) is a total order for \(S\).
Examples:
\begin{itemize}
    \item \(\leq\) is a total order for \(\mathbb{R}\)
    \item \(divides\) is not a total order since \(divides(2, 5) = divides(5, 2) = F\)
\end{itemize}
A partial order \(R\) for \(S\) is a well-ordering of \(S\) if every nonempty subset of \(S\) has a smallest element, i.e.
\[\forall T \in \mathcal{P}(S) \exists m \in T, \forall x \in T, R(m, x)\]
Examples:
\begin{itemize}
    \item \(\leq\) is a well ordering of \(\nats\)
    \item \(\leq\) is not a well-ordering of \(\ints\) (\(\ints\) has no smallest element)
\end{itemize}
Consider the ordering \(\sless\) of \(\ints\) where
\[x \sless y \iff (|x| < |y| \oor (|x| = |y| \aand x < y))\]
\[0 \sless -1 \sless 1 \sless -2 \sless 2...\]
As we can see, \(\sless\) is a well-ordering of \(\ints\).

To prove \(\forall e \in S, P(e)\) where \(\sless\) is a well-ordering of \(S\)
\begin{itemize}

    \item[L1] To obtain a contradiction, suppose \(\forall e \in S, P(e)\) is false
    \item[L2] Let \(C = \{e \in S|P(e) = F\}\) be the set of counter examples to \(P\)
    \item[L3] \(C \neq \varnothing\) by L1
    \item[L4] Let \(e\) be the smallest element of \(C\), which must exist since \(C\) is nonempty
    \item[]\begin{itemize}
        \item[L5] Let \(e' = ...\)
        \item[] \(\vdots\)
        \item[L6] \(e' \in C\)
        \item[L7] \(e' \neq e\)
        \item[L8] \(e' \leq e\)
    \end{itemize}
    \item[L9] This is a contradiction, as \(e'\) is smaller than \(e\), so \(e\) cannot be the smallest element in \(C\)
\end{itemize}
\(\forall e \in S, P(x)\) proof by contradiction (this is called a well-ordering proof in the MIT book).

\begin{theorem}
Every element of \(\mathbb{Q}^+\) can be expressed in reduced form, i.e. as \(m'/n'\) where \(m', n'\) have no common factors.
\end{theorem}
\begin{proof}
Suppose there exist \(m, n \in \mathbb{Z}^+\) such that \(m/n\) cannot be expressed in reduced form. Let 
\[c = \{m \in \mathbb{Z}^+|\exists n \in \mathbb{Z}^+, m/n \text{ cannot be expressed in reduced form}\}\]
By assumption, \(c \neq 0\). Since \(\mathbb{Z}^+\) is well ordered, \(C\) must have a smallest element \(m_0\) such that \(\exists n_0\) such that \(m_0/n_0\) cannot be expressed in reduced form. In particular \(m_0\) and \(n_0\) have a common factor \(p\). Let \(m_0' = m_0/p\) and \(n_0' = n_0/p\). Since \(m_0'/n_0' = m_0/n_0\), it cannot be expressed in reduced form. So \(m_0' \in C\). But \(m_0' < m_0\). contradicting the fact that \(m_0\) is the smallest element in \(C\), yielding a proof by contradiction.
\end{proof}
Countable and Uncountable sets:
\[f: A \to B\]
is \underline{surjective} or \underline{onto} if
\[\forall b \in B, \exists a \in A, f(a) = b\]
If \(A\) and \(B\) are finite, we can conclude \(|A| \geq |B|\). A nonempty set \(C\) is \underline{countable} if there exists a surjective function from \(\nats\) to \(C\). Every nonempty finite set is countable, because we can just number the elements (since they are finite), and do whatever we want with the ``rest'' of the natural numbers, say map them all to one element, or use modular arithmetic.

If \(C = \varnothing\) is there a surjective function from \(\nats\) to \(C\). The answer is no, because there's no element in the co-domain to map elements of the domain to. The empty set iscountable too (by definition). 

\(\mathbb{Z}\) is countable, as we can write \(0, 1, -1, 2, -2, 3...\). \((\mathbb{N} \times \mathbb{N})\) is countable too: we can ``count out'' our infinite grid diagonally!

Lemmas:
\begin{itemize}
    \item If \(A, B\) are countable then \((A \cup B), (A \times B)\) are countable
    \item If \(A\) is countable and \(B \subseteq A\), then \(B\) is countable (this implies that if \(B\) is any set, \(A \cap B \subseteq A\) is countable)
\end{itemize}
\end{document}