\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC240: Analysis of Algorithms}
\author{Jad Elkhaleq Ghalayini}
\date{March 5 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{tabto}

\def\nats {{\mathbb N}}
\def\ints {{\mathbb Z}}
\def\reals {{\mathbb R}}
\newcommand{\iimp}{\mbox{ IMPLIES }}
\newcommand{\oor}{\mbox{ OR }}
\newcommand{\aand}{\mbox{ AND }}
\newcommand{\nnot}{\mbox{ NOT }}
\newcommand{\iiff}{\mbox{ IFF }}

\newtheorem{theorem}{Theorem}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

Why might we want to know how fast an algorithm runs?
\begin{itemize}
    \item To estimate how long an algorithm will run on a given input
    \item Determine how large an input is reasonable to give the algorithm
    \item To compare algorithms
\end{itemize}
In general, the actual running time of an algorithm depends on a lot of factors, such as coding quality, the compiler that you use, what machine the algorithm is implemented for and how heavily loaded the machine is at the time. When we analyze an algorithm, we really only estimate the running time to within a constant factor. There is some research about actually looking at the actual timing of various operations in the hardware and extrapolating from there about efficiency, but it was definitely an academic paper, and it was only done once. You can't generally analyze an algorithm to more than a constant factor, unless you have all the other parameters fixed. 

For an algorithm \(A\), let \(t_A(I)\) denote the number of steps the algorithm takes on input \(I\). What's a step? One thing we could do is count every single addition, array index, assignment, etc. But remember, we're only estimating to within a constant factor, so we don't need to be that accurate. Typically, we pick one or two operations, such that the number of these operations performed is the same as the total number of operations within a constant factor. Now if your algorithm performs only one multiplication, but lots and lots of additions, then counting only multiplications is probably a poor choice. You want to pick something relevant for that algorithm.

This is an algorithm for doing linear search:
\begin{itemize}
    \item []\(LS(L, x)\)
    \item []\(i \leftarrow 1\)
    \item []while \(i \leq \mbox{length}(l)\) do
    \item []\begin{itemize}
        \item []if \(L[i] = x\) then return \(i\)
        \item []\(i \leftarrow i + 1\)
    \end{itemize}
    \item []end while
    \item []return 0
\end{itemize}
The running time of a program usually increases as the size of the input gets larger. Use a function \(T(n)\) of the input size \(n\) to represent the running time of an algorithm on inputs of size \(n\).

\begin{itemize}

    \item Worst case time complexity: \[T_A: \ints^+ \to \nats, T_A(n) \mapsto \max\{t_A(I) | size(I) = n\}\]
    
    \item Average case time complexity: \[T_A: \ints^+ \to \reals^+, T_A'(n) = E[t_A]\]
    where expectation is taken over a probability space of all inputs of size \(n\). If all inputs are equally likely,
    \[T_A'(n) = \frac{\sum\{t_A(I) | size(I) = n\}}{\#\{t_A(I) | size(I) = n\}}\]
    
\end{itemize} 
    In this course, we'll generally only do worst-case time complexity, as average-case analysis is significantly more complex.

    Sometimes we express running time as a function of 2 or more parameters, e.g.
    \[T_G(n, m)\]
    where \(n = \#\text{nodes}\), \(m = \#\text{edges}\) for a graph algorithm \(G\). Consider algorithm s\(A, B\) such that
    \[T_A(n) = n^3, T_B(n) = 8n + 3\]
    For input sizes less than 3, \(T_A\) is faster, whereas whenever \(n\) is greater than or equal to 3, \(T_B\) becomes much faster very quickly. 3 is called the \textit{breakpoint}.
    
    To prove that \(T_A \leq u\), we must show that, for all \(n \in \ints^+\), for all inputs \(I\) of size \(n\),
    \[t_A(I) \leq u(n)\]
    To show that \(u\) is a good upper bound, we can get a lower bound \(l\) on the worst case running time of the algorithm. If \(l, u\) are close, we know that we've actually sandwiched \(T_a\) between them.
    To prove that \(T_A \geq l\), we must show that, for all \(n \in \ints^+\), there exists an input \(I\) of size \(n\),
    \[t_A(I) \geq l(n)\]
    DO NOT use \(O\) notation for lower bounds! \(O\)-notation is very useful for talking about upper bounds, but not about lower bounds, since it talks about functions growing faster than others. For lower bounds, we use ``big-omega'', \(\Omega\), notation:
    \[\Omega(f) = \{g \in \mathcal{F} | \exists c \in \reals^+, \exists b \in \nats, \forall n \in \ints^+, n \geq b \iimp g(n) \geq c \cdot f(n)\}\]
    Something else we may use is ``big-theta'', \(\theta\), notation:
    \[\theta(f) = \{g \in \mathcal{F} | \exists c_1 \in \reals^+, \exists c_2 \in \reals^+, \exists b \in \nats, \forall n \in \ints+,\]\[ n \geq b \iimp c_1 \cdot f(n) \leq g(n) \leq c_2 \cdot f(n)\}\] 
\end{document}