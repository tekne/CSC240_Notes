\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC240: Recurrences}
\author{Jad Elkhaleq Ghalayini}
\date{February 26 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{tabto}

\def\nats {{\mathbb N}}
\def\ints {{\mathbb Z}}
\def\reals {{\mathbb R}}
\newcommand{\iimp}{\mbox{ IMPLIES }}
\newcommand{\oor}{\mbox{ OR }}
\newcommand{\aand}{\mbox{ AND }}
\newcommand{\nnot}{\mbox{ NOT }}
\newcommand{\iiff}{\mbox{ IFF }}
\newcommand{\sless}{S}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

A recurrence is an inductively defined function. For example, let
\[T: \ints^+ \to \nats\]
be such that
\[T(n) = \left\{\begin{array}{ccc} 0 & \text{if } n = 1 & \text{(base case/initial condition)} \\ 4 + T(n - 1) & \text{if } n > 1 & \text{(constructor case/self-referential part)}\end{array}\right.\]
Solving a recurrence: find a non-recursive description. Methods:
\begin{enumerate}
    \item Look it up: Doesn't work very well on tests, unless its on your aid sheet
    \item Guess and verify (typically by induction).
    \begin{itemize}
        \item Generate a table of values for the function
        \item Look for a pattern
        \item Guess a solution
    \end{itemize}
    \item Chug \& plug/ repeated substitution and verify (by induction).
    
    Example: for \(n \in \ints^+\), let
    \[M(n) = \left\{\begin{array}{cc}c & \text{if } n = 1 \\ M(\ceil{n/2}) + M(\floor{n/2}) + dn & \text{if } n > 1\end{array}\right.\]
    where \(c, d \in \nats\) are constants.
    
    If \(c = d = 0\), then \(M(n) = 0\).
    \[M(n) = \left\{\begin{array}{cc}c & \text{if } n = 1 \\ 2M(n/2) + dn & \text{if } n > 1\end{array}\right.\]
    if \(n\) is a power of 2.
    Substituting, we get
    \[M(n) = 2M(n/2) + dn = 4M(n/4) + 2dn = 8M(n/8) + 3dn...\]
    And eventually, we come to the guess that
    \[M(n) = cn + dn\log_2n\]
    Now, to verify:
    \[\text{Let } Q(k) = \text{``}M(2^k) = c2^k + dk2^k\text{''}\]
    \[\forall k \in \nats, Q(k)\]
    \begin{theorem}
        \(M(n) \in O(n\log n)\)
    \end{theorem}
        First show that \(M\) is a nondecreasing function i.e. 
        \[m < n \iimp M(m) \leq M(n)\]
        For \(n \in \ints^+\), let \(R(n)\) denote the predicate
        \[\forall m \in \ints^+, (m \leq n \iimp M(m) \leq M(n)\]
        \begin{lemma}
        \(\forall n \in \ints^+, R(n)\)
        \end{lemma}
        \begin{proof}
        Let \(n \in \ints^+\) be arbitrary and suppose \(R(n)\) is true for all \(1 \leq n' < n\).
        \begin{itemize}
            \item[] \(R(1)\) is vacuously true
            \item[] \(M(1) = c \leq 2c + 2d = M(2)\) so \(R(2)\) is true
            \item[] Now consider the case \(n > 2\):
            \[1 \leq \floor{n/2} \leq \ceil{n/2} \leq n - 1 < n\]
            Thus, by the inductive hypothesis,
            \[R(\floor{n/2}), R(\ceil{n/2}), R(n - 1)\]
            Let \(m \in \ints^+\) be arbitrary and suppose \(m < n\).
            \begin{itemize}
                \item [Case 1:] \(m = n - 1\): if \(n\) is odd,
                \[\ceil{\frac{n - 1}{2}} < \ceil{\frac{n}{2}}, \ \floor{\frac{n - 1}{2}} = \floor{\frac{n}{2}}\]
                By the IH,
                \[M\left(\ceil{\frac{n - 1}{2}}\right) \leq M\left(\ceil{\frac{n}{2}}\right), \ M\left(\floor{\frac{n - 1}{2}}\right) = M\left(\floor{\frac{n}{2}}\right)\]
                \[M(n - 1) = M\left(\ceil{\frac{n - 1}{2}}\right) + M\left(\floor{\frac{n - 1}{2}}\right) + d(n - 1) \leq M\left(\ceil{\frac{n}{2}}\right) + M\left(\floor{\frac{n}{2}}\right) + dn = M(n)\]
                \item There is a similar proof if \(n\) is even.
            \end{itemize}
        \end{itemize}
        \end{proof}
        We can now begin to prove the main result:
        \begin{proof}
        Let \(n \in \ints^+\) be arbitrary. Let \(2^k\) be the smallest power of 2 that is greater than or equal to \(n\).
        \[n \leq 2^k < 2n\]
        By the lemma, or if \(n = 2^k\),
        \[M(n) \leq M(2^k) = c2^k + dk2^k < 2cn + 2d\log_2(2n) = 2cn _2dn(\log_2(n) + 1)\]
        So
        \[M(n) \in O(n\log n)\]
        \end{proof}
        
    There's a more general version of this:
    \begin{theorem}[Master Theorem]
    For \(n \in \ints^+\), let
    \[T(n) = \left\{\begin{array}{cc} c & \text{if } n < B \\ a_1T(\ceil{n/b}) + a_2T(\floor{n/b}) + dn^\ell & \text{if } n \geq B\end{array}\right.\]
    Where \(a_1, a_2, B \in \nats, a = a_1 + a_2 \geq 1, b > 1, c, d, \ell \in \reals^+ \cup \{0\}\)
    Then
    \[T(n) \in \left\{\begin{array}{cc}O(n^\ell\log n) & \text{if } a = b^\ell \\ O(n^\ell) & \text{if } a < b^\ell \\ O(n^{\log_b a}) & \text{if } a > b^\ell\end{array}\right.\]
    
    \item Transformations:
    \[G(n) = \left\{\begin{array}{cc}2G(n/2) + \log_2(n) & \text{if } n > 1 \\ 0 & \text{if } n = 1\end{array}\right.\]
    Let \(n = 2^\ell, H(\ell) = G(2^\ell)\).
\end{theorem}
\end{enumerate}
\end{document}