\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC240: Correctness of Algorithms}
\author{Jad Elkhaleq Ghalayini}
\date{March 12 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{tabto}
\usepackage{listings}

\def\nats {{\mathbb N}}
\def\ints {{\mathbb Z}}
\def\reals {{\mathbb R}}
\newcommand{\iimp}{\mbox{ IMPLIES }}
\newcommand{\oor}{\mbox{ OR }}
\newcommand{\aand}{\mbox{ AND }}
\newcommand{\nnot}{\mbox{ NOT }}
\newcommand{\iiff}{\mbox{ IFF }}
\newcommand{\recurrence}[4]
{\left\{
    \begin{array}{ll}
    #1 & \mbox{ if } #2 \\ #3 & \mbox{ if } #4
    \end{array}
\right.}


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}
\newtheorem{corollary}{Corollary}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

An algorithm is correct if it satisfies its specifications. Specifications are often written using preconditions and postconditions. A \underline{precondition} is a statement involving the variable used in the algorithm. It  says that certain facts must be true before an execution of the algorithm begins. It can describe the allowable inputs. A \underline{postcondition} is also a statement about the variables used in the algorithm. It says that certain facts must be true when an execution of the algorithm ends. It often describes the correct output or outputs for a given input.
\begin{itemize}
    \item [Termination:] the algorithm halts, provided the preconditions hold
    \item [Partial correctness:] if the preconditions hold, the algorithm is executed and if it eventually halts, then the postconditions hold
    \item [Total correctness:] Partial correctness and termination
\end{itemize}
Example: specifications for the search of an array \(A\) for key \(k\)
\begin{itemize}
    \item [Preconditions:] \(A\) is an array and \(k\) is an element of the same type as elements of \(A\)
    \item [Postconditions:] Return an integer \(i\) such that \(1 \leq i \leq length(A)\) such that \(A[i] = k\) if such \(i\) exists and 0 otherwise.
\end{itemize}
Consider the algorithms:
\begin{lstlisting}[language=Python]
def algorithm_1(A, k):
    if len(A) > 0:
        A[1] = k
        return 1
    return 0
    
def algorithm_2(A, k):
    if len(A) > 0:
        k = A[1]
        return 1
    return 0
\end{lstlisting}
These both satisfy the postconditions, but they're pretty bad algorithms. We need to modify the specification to assert that we can't modify \(A\).

Examples: 
\begin{itemize}

    \item Binary search
    \begin{itemize}
        \item [Preconditions:] \(A\) is sorted in non-decreasing order, \(k\) is a key of the same type as the elements of \(A\)
        \item [Postconditions:] \(A\) is unchanged, return an integer \(i\) such that \(1 \leq i \leq length(A)\) such that \(A[i] = k\) if such \(i\) exists and 0 otherwise.
    \end{itemize}
    
    \item Sorting
    \begin{itemize}
        \item [Preconditions:] \(A\) is an array of comparable elements
        \item [Postconditions:] \(A\) contains the same elements as before (i.e. the multiset of elements in \(A\) is unchanged) rearranged so as to be in nondecreasing orde
    \end{itemize}
    
    \item Merging an array
    \begin{itemize}
        \item [Preconditions:] \(A, B\) are arrays sorted in nondecreasing order
        \item [Postconditions:] The resulting array \(C\) contains all elements in \(A, B\) (i.e. the multiset of elements in \(C\) is equal to the multiset union of the multiset of elements in \(A\) and the multiset of elements in \(B\)) and is sorted in nondecreasing order
    \end{itemize}
    
\end{itemize}
Now, we're going to do a proof of correctness
\begin{lstlisting}[language=Python]
def mergesort(A, N):
    if n > 1:
        m = n//2
        U = A[1:m]
        V = A[m:n]
        mergesort(U, m)
        mergesort(V, n - m)
        A[:] = merge(U, V)
\end{lstlisting}
Now, we can do a proof of correctness:
\begin{proof}
For \(n \in \nats\), let \(P(n)\) denote the predicate ``for all arrays \(A[1...n]\) with elements from a totally ordered domain, if \(mergesort(A, n)\) is performed, then it eventually halts at which time \(A\) is sorted in nondecreasing order an the multiset of elements in \(A\) is unchanged.'' We want to prove \(\forall n \in \nats, P(n)\).

Let \(n \in \nats\) be arbitrary. Let \(A[1...n]\) be an arbitrary array of elements from a totally ordered domain. Suppose \(P(n')\) is true for all \(n' \in \nats\) with \(n' < n\). 
\begin{itemize}
    \item [Base cases:] \(n = 0, 1\): the test on line 1 fails, the algorithm halts and \(A\) is unchanged. \(A\) is vacuously sorted in nondecreasing order. By generalization, \(P(n)\) is true.
    \item [Induction step:] Suppose \(n > 1\). The test on line 1 succeeds. \(m = n//2 = \floor{n/2} < n \implies n - m \in \nats, n - m < n\). By the inductive hypothesis, after \(mergesort(U, m)\) and \(mergesort(V, n - m)\), \(U, V\) are sorted in nondecreasing order and the multiset of elements in each is unchanged. After applying merge, \(A\) is in nondecreasing order and the multiset of the elements of \(A\) is equal to the multiset of elements in \(U\) combined with the multiset of elements in \(V\), which is equal to the multiset of \(A\). So \(P(n)\)
\end{itemize}
Hence, by strong induction, \(\forall n \in \nats, P(n)\)
\end{proof}
Let's try quicksort:
\begin{lstlisting}[language=Python]
def quicksort(A):
    if len(A) > 1:
        pivot = A[1]
        L = [x for x in A if x < pivot]
        E = [x for x in A if x == pivot]
        G = [x for x in A if x > pivot]
        quicksort(L)
        quicksort(G)
        A[:] = L + E + G
\end{lstlisting}
The proof of correctness is going to be very similar to that for mergesort.
\end{document}